
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation

INTRODUCTION
------------

Current Maintainer: Bastlynn bastlynn@gmail.com

PURPOSE

INSTALLATION
------------

1. Unzip and copy this directory to your sites/SITENAME/modules directory. 

2. Enable the module. 

3. Assign site specific GA tags under the Sites admin interface.